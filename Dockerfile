# pull the base image
FROM node:alpine
# set the working direction
WORKDIR /app
# install app dependencies
COPY package*.json ./

RUN npm install
# add app
COPY . ./

ENV PORT=8080

EXPOSE 8080
# start app
CMD ["npm", "start"]
